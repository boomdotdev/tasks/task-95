import "./scss/app.scss";
import Application from "./js/Application";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  const app = new Application();
  app.setProducts([
    {
      item: "🍎",
      price: 10,
    },
    {
      item: "🍌",
      price: 20,
    },
    {
      item: "🍇",
      price: 30,
    },
  ]);
  app.sumTotalPrice();
  // Used to access the app instance by the automated tests
  window.__JS_APP = app;
});
