import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.products = [];
    this.emit(Application.events.READY);
  }

  setProducts(products) {
    this.products = products;
    const shoppingCart = document.querySelector(".shopping-cart");
    for (let i = 0; i < products.length; i++) {
      const productWrapper = document.createElement("div");
      const itemParagraph = document.createElement("p");
    }
  }

  sumTotalPrice() {}
}
